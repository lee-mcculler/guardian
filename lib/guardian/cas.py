import os
import copy
import pcaspy
import threading

from . import const
from .db import guarddb

# add the REQUEST_ENUM interface channel
guarddb['REQUEST_ENUM'] = {
    'type': 'enum',
    'enums': (),
    'writable': True,
    }

class CAServerError(Exception):
    pass

class CADriver(pcaspy.Driver):
    def __init__(self, request_event):
        super(CADriver, self).__init__()
        self._state_index = {}
        self._request_event = request_event

    ##########

    def __getitem__(self, channel):
        value = self.getParam(channel)
        if guarddb[channel]['type'] == 'enum':
            if guarddb[channel].get('guardbool', False) and value == 0:
               value = False
            else:
                value = guarddb[channel]['enums'][value]
        return value

    def __setitem__(self, channel, value):
        # handle state readbacks
        if guarddb[channel].get('guardstate', False):
            nvalue = self._state_index[value]
            self.setParam(channel+'_S', value)
            self.setParam(channel+'_N', nvalue)
        # resolve enums
        if guarddb[channel]['type'] == 'enum':
            value = guarddb[channel]['enums'].index(str(value))
        # update REQUEST_ENUM
        if channel == 'REQUEST' and value in guarddb['REQUEST_ENUM']['enums']:
            self.setParam('REQUEST_ENUM',
                          guarddb['REQUEST_ENUM']['enums'].index(str(value)))
        self.setParam(channel, value)

    ##########

    def write(self, channel, value):
        # NOTE: for enum records the value here is the numeric value,
        # not the string.  setParam() expects the numeric value.

        # reject writes to non-writable channels
        if not guarddb[channel].get('writable', False):
            return False

        # reject values that don't correspond to an actual index of
        # the enum
        # FIXME: this is apparently a feature? of cas that allows for
        # setting numeric values higher than the enum?
        if guarddb[channel]['type'] == 'enum' \
           and value >= len(guarddb[channel]['enums']):
            return False

        if channel == 'REQUEST':
            # reject invalid states
            if value not in self._state_index.keys():
                return False
            self['REQUEST'] = value
            self._request_event.set()

        elif channel == 'REQUEST_ENUM':
            self['REQUEST'] = guarddb[channel]['enums'][value]
            self._request_event.set()

        elif channel == 'MODE':
            # reject MANAGED request
            # FIXME: how to move this out of here?
            if value == 1:
                return False
            self.setParam(channel, value)

        elif channel == 'MANAGER':
            # reject setting manager to empty string
            if value == '':
                return False
            self.setParam(channel, value)

        else:
            self.setParam(channel, value)

        # HACK: the setParam method sets pv flag=True, which indicates
        # that the value needs to be writen out on the next
        # updatePVs().  This is usually not needed during write(),
        # since the values being written are already externally
        # up-to-date when write() is executed.  However, since we're
        # updating auxilliary records during write() (e.g. state
        # readbacks), we need to run updatePVs() for those aux
        # channels to be updated.  This then causes the write()
        # channels to be double updated, which in turn causes
        # connected clients to see two updates.  Setting flag=False
        # for the original write channel prevents this double update
        # from happening.  This should probably be fixed upstream.
        # UPDATE: this does not seem to work in pcaspy 0.7, and
        # instead causes channels to not show updates properly
        #self.pvDB[channel].flag = False
        self.updatePVs()
        return True


class CAServer(threading.Thread):
    def __init__(self, system_name):
        super(CAServer, self).__init__()
        self.prefix = const.CAS_PREFIX_FMT.format(IFO=const.IFO, SYSTEM=system_name)
        self._ready_event = threading.Event()
        self._request_event = threading.Event()
        self._server = pcaspy.SimpleServer()
        #self._server.setDebugLevel(4)
        self._loaddb()
        self._driver = CADriver(self._request_event)
        # clear undefined alarms at startup
        for chan in guarddb:
            self._driver.setParamStatus(chan, pcaspy.Severity.NO_ALARM, pcaspy.Severity.NO_ALARM)
        self._driver.updatePVs()
        self._running = True
        self.daemon = True

    def _loaddb(self):
        self._server.createPV(self.prefix, guarddb)

    ########################################

    def __getitem__(self, channel):
        return self._driver[channel]

    def __setitem__(self, channel, value):
        self._driver[channel] = value
        self._driver.updatePVs()

    ########################################

    def update_state_index(self, index, init=False):
        """Update state index dictionary"""
        self._driver._state_index = index
        if init:
            return
        # update all guardstate channels, in case the state:index
        # mapping has changed
        for channel, entry in guarddb.iteritems():
            if entry.get('guardstate', False):
                self[channel] = self[channel]

    def update_request_enum(self, enum, init=False):
        """Update request enum tuple"""
        if enum != guarddb['REQUEST_ENUM']['enums']:
            guarddb['REQUEST_ENUM']['enums'] = enum
            self._driver.setParamEnums('REQUEST_ENUM', enum)
            self._driver.updatePVs()
            if not init:
                # update request enum, in case it's changed
                self['REQUEST_ENUM'] = self['REQUEST']
            return True
        return False

    ########################################

    @property
    def new_request(self):
        val = self._request_event.is_set()
        if val:
            self._request_event.clear()
        return val

    ########################################

    def start(self):
        super(CAServer, self).start()
        self._ready_event.wait()

    def stop(self):
        self._running = False

    def run(self):
        self._ready_event.set()
        while self._running:
            self._server.process(const.TIME_STEP)
