import time
import threading

class Clock(threading.Thread):
    """daemon run clock.

    Caller should call wait() method to be released on every cycle.

    """
    def __init__(self, time_step):
        super(Clock, self).__init__()
        self._time_step = time_step
        self.daemon = True
        # clock cycle condition object
        self._cv = threading.Condition()
        self._cv.daemon = True
        # run flag
        self._running = True

    def stop(self):
        self._running = False

    def run(self):
        while self._running:
            time.sleep(self._time_step)
            with self._cv:
                 self._cv.notify_all()

    def wait(self):
        """Wait for the next tick of the timer"""
        with self._cv:
            self._cv.wait()
