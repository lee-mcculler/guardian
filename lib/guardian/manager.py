import os

from .state import GuardStateDecorator

class NodeError(Exception):
    pass

class NodeConnectError(Exception):
    pass

# FIXME: handle rejected requests
# FIXME: check for dead node

class Node(object):
    """Manager interface to a single Guardian node.

    >>> SUS_ETMX = Node('SUS_ETMX')  # create the node object
    >>> SUS_ETMX.init()              # initialize (handled automatically in daemon)
    >>> SUS_ETMX.set_managed()       # set node to be in MANAGED mode
    >>> SUS_ETMX.set_request('DAMPED') # request DAMPED state from node
    >>> SUS_ETMX.arrived             # True if node arrived at requested state

    """

    # node attributes
    attrs=('OP',
           'MODE',
           'MANAGER',
           'REQUEST',
           'STATE_S',
           'TARGET_S',
           'REQUEST_S',
           'STATUS',
           'STALLED',
           'ERROR',
           'NOTIFICATION',
           )

    def __init__(self, name):
        self.__name = name
        self.__prefix = ':GRD-{name}_'.format(name=self.name)
        # cache that we have set node to be managed
        self.__managed = False
        # cache of requested state
        self.__request = None
        # initialization state
        self.__initialized = False

    def __repr__(self):
        return "%s(%r)" % (self.__class__.__name__, self.__name)

    def __str__(self):
        if self.__initialized:
            s = 'initialized'
        else:
            s = 'uninitialized'
        return "<%s '%s', %s>" % (self.__class__.__name__, self.__name, s)

    @property
    def name(self):
        """Node name"""
        return self.__name

    def __get(self, attr):
        """get attribute of node."""
        return ezca.read(self.__prefix+attr, as_string=True)

    def __put(self, attr, value):
        """set attribute of node."""
        assert attr in ['MANAGER', 'REQUEST'], "MANAGER and REQUEST are the only settable node attributes."
        return ezca.write(self.__prefix+attr, value)

    ########################################

    def init(self):
        """Initialize the node.

        Under normal circumstances, i.e. in a running guardian daemon,
        node initialization is handled automatically.  This function
        therefore does not need to be executed in user code.

        """
        # FIXME: could elliminate this enitrely by just using the rote
        # ezca object
        if self.__initialized:
            return

        # initialize node PVs
        for attr in self.attrs:
            name = self.__prefix + attr
            if name in ezca._dev._pvs:
                continue
            # fullname = _IFO_ + name
            # ezca._dev.add_pv(fullname, name)
            ezca.connect(name)

        # if the node is currently set to be managed by me
        if self.i_manage:

            # update the internal manage indicator
            self.__managed = True

            # set the internal request indicator based on the current
            # request (assuming it was set by me)
            if not self.__request:
                self.__request = self.REQUEST

        self.__initialized = True

    def __eq__(self, state):
        """True if node state string equals string."""
        return self.state == state

    def __ne__(self, state):
        """True if node state string does not equal string."""
        return not self.__eq__(state)

    @property
    def OP(self):
        """node OP"""
        return self.__get('OP')

    @property
    def MODE(self):
        """node MODE"""
        return self.__get('MODE')

    @property
    def managed(self):
        """True if node is MANAGED"""
        return self.MODE == 'MANAGED'

    @property
    def MANAGER(self):
        """MANAGER string of node"""
        return self.__get('MANAGER')

    def set_managed(self):
        """Set node to be managed by this manager."""
        # note that we have set node to be managed
        self.__managed = True
        self.__put('MANAGER', _SYSTEM_)

    @property
    def i_manage(self):
        """True if node is being managed by this system"""
        return self.MANAGER == _SYSTEM_

    @property
    def ERROR(self):
        """True if node in ERROR."""
        return eval(self.__get('ERROR'))

    @property
    def NOTIFICATION(self):
        """True if node NOTIFICATION present."""
        return eval(self.__get('NOTIFICATION'))

    @property
    def OK(self):
        """Current OK status of node."""
        return eval(self.__get('OK'))

    @property
    def REQUEST(self):
        """Current REQUEST state of node."""
        return self.__get('REQUEST_S')
    request = REQUEST

    def set_request(self, state):
        """Set REQUEST state for node.

        NOTE: this is a NOOP if node is *NOT* in MANAGED mode.

        """
        if not self.__managed:
            raise NodeError("can not set REQUEST unless node has been set_managed().")
        # only issue the request if the subordinate is running, is in
        # manager mode, and it's managed by me
        if self.OP == 'EXEC' \
           and self.managed \
           and self.i_manage:
            self.__put('REQUEST', state)
        # note that we've set the request internally, even if we
        # didn't actually issue the request to the node because of the
        # above restriction
        self.__request = state
        # FIXME: set call back to record when subordinate makes it to
        # request?

    @property
    def STATE(self):
        """Current STATE of node."""
        return self.__get('STATE_S')
    state = STATE

    @property
    def TARGET(self):
        """Current TARGET state of node."""
        return self.__get('TARGET_S')

    @property
    def arrived(self):
        """True if node STATE equals the last manager-requested state.

        NOTE: This will be False if STATE == REQUEST but REQUEST was
        not last set by this Node manager object.  This prevents false
        positives in the case that the REQUEST has been changed out of
        band.

        """
        # FIXME: this should be a latching value.  it should be reset
        # when the request is first set, and set True once it arrives.
        # it should then not change when it looses the requested
        # state.  maybe that behavior should just be added to the core
        return self.STATE == self.__request

    @property
    def STATUS(self):
        """Current STATUS of node."""
        return self.__get('STATUS')

    @property
    def done(self):
        """True if STATUS is DONE.

        A state is DONE if it is the requested state and the state
        method has returned True.

        """
        return self.STATUS == 'DONE'

    @property
    def STALLED(self):
        """True if the node has stalled in the current state.

        This is true when STATE == TARGET != REQUEST, which is
        typically the result of a jump transition while in managed
        mode.

        """
        return eval(self.__get('STALLED'))

    def revive(self):
        """Re-request last requested state.

        The last requested state in this case is the one requested
        from this Node object.

        Useful for reviving stalled nodes, basically counteracting the
        stalling that is the effect of a jump transition while being
        in MANAGED mode.  See the 'stalled' property.

        """
        self.set_request(self.__request)

    def check_fault(self):
        """Return fault status of node.

        Returns tuple: (fault, message)
        * 'fault' is a bool to indicate a fault is present.
        * 'message' is a list of notification messages.

        The following checks are run:
        * CA connections active (node alive)
        * no ERROR
        * REQUEST hasn't deviated from last set value

        'fault' is True if any of the above are False.  In addition,
        the following produce notification 'message's but not faults:
        * not MANAGED
        * NOTIFICATION present

        """
        fault = False
        msg = []

        if self.ERROR:
            msg.append("ERROR!")
            fault = True

        if self.OP != 'EXEC':
            msg.append("not EXEC")
            fault = True

        if self.NOTIFICATION:
            msg.append("NOTIFICATION")

        if self.__managed:
            if not self.managed:
                msg.append("NO LONGER MANAGED")
                fault = True

            elif not self.i_manage:
                msg.append("STOLEN (by: %s)" % self.MANAGER)
                fault = True

            request = self.REQUEST

            if self.__request and request != self.__request:
                msg.append("REQUEST CHANGED (was: %s, now: %s)" % (self.__request, request))
                fault = True

        return fault, msg

class NodeManager(object):
    """Manager interface to a set of subordinate Guardian nodes.

    This should be instantiated with a list of node names to be
    managed.  Node objects are instantiated for each node.

    >>> nodes = NodeManager(['SUS_ITMX','SUS_ETMX'])
    >>> nodes.init()                   # initialize (handled automatically in daemon)
    >>> nodes.set_managed()            # set all nodes to be in MANAGED mode
    >>> nodes['SUS_ETMX'] = 'ALIGNED'  # request state of node
    >>> nodes['SUS_ITMX'] = 'ALIGNED'  # request state of node
    >>> nodes.arrived                  # True if all nodes have arrived at their
                                       # requested states

    """
    def __init__(self, nodes):
        self.nodes = {}
        for node in nodes:
            self.nodes[node] = Node(node)

    def __repr__(self):
        return "%s(%r)" % (self.__class__.__name__, self.nodes.keys())

    def __str__(self):
        return "<%s %s>" % (self.__class__.__name__, self.nodes.keys())

    def __getitem__(self, node):
        """Retrieve Node object for named node."""
        return self.nodes[node]

    def __setitem__(self, node, state):
        """Request state for named node."""
        try:
            self.nodes[node].set_request(state)
        except NodeError as e:
            raise NodeError("%s: %s" % (node, e))

    def __iter__(self):
        """Iterator of node objects."""
        for node in self.nodes.itervalues():
            yield node

    def init(self):
        """Initialize all nodes.

        Under normal circumstances, i.e. in a running guardian daemon,
        node initialization is handled automatically.  This function
        therefore does not need to be executed in user code.

        """
        for node in self:
            node.init()

    def set_managed(self):
        """Set all nodes to be managed by this manager."""
        for node in self:
            node.set_managed()

    @property
    def arrived(self):
        """Return True if all nodes have arrived at their requested state."""
        for node in self:
            if not node.arrived:
                return False
        return True

    def get_stalled_nodes(self):
        """Return a list of all stalled nodes."""
        return [node for node in self if node.STALLED]

    def revive_all(self):
        """Revive all stalled nodes."""
        for node in self:
            if node.STALLED:
                log("Reviving stalled node: %s" % node.name)
                node.revive()

    def check_fault(self):
        """Check fault status of all nodes.

        Runs check_fault() method for all nodes.  Returns True if any
        nodes are in fault.  Any messages are displayed as
        notifications.

        """
        any_fault = False
        all_msg = []
        for node in self:
            fault, msgs = node.check_fault()
            any_fault |= fault
            for msg in msgs:
                notify("node %s: %s" % (node.name, msg))
        return any_fault

    def checker(self, fail_return=None):
        """Return GuardStateDecorator for checking fault status of all nodes.

        The GuardStateDecorator pre_exec is set to be the
        NodeManager.check_fault method.  The option "fail_return"
        argument is a return value for the decorator in case the check
        fails (i.e. a jump state name) (default None).

        """
        # we want to return a GuardStateDecorator here, where the
        # pre_exec function is the NodeManager.check method.  since
        # "self" here is the NodeManager, we tell pre_exec to just
        # ignore it's argument, which would have been the
        # GuardStateDecorator self.
        class checker(GuardStateDecorator):
            def pre_exec(__):
                if self.check_fault():
                    return fail_return
        return checker
