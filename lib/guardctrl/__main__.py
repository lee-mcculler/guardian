import os
import sys
import click
import signal
import logging
import functools
import subprocess
import collections
from datetime import timedelta
from gpstime import gpstime, GPSTimeException
from dateutil.tz import tzutc, tzlocal

from . import systemd
from . import svlogd
from . import util

##################################################

PROG = 'guardctrl'

try:
    IFO = os.environ['IFO']
except KeyError:
    raise click.ClickException("Must specify IFO.")
try:
    SITE = os.environ['SITE']
except KeyError:
    raise click.ClickException("Must specify SITE.")


def print_version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo('Version ???')
    ctx.exit()

##################################################

# hack to override alphabetic sorting of commands
class GuardctrlCommandGroup(click.Group):
    def __init__(self, *args, **kwargs):
        click.Group.__init__(self, *args, **kwargs)
        self.commands = collections.OrderedDict()

    def list_commands(self, ctx):
        return self.commands.keys()

    def get_command(self, ctx, cmd):
        if cmd in ['node', 'nodes']:
            cmd = 'list'
        elif cmd in ['create']:
            cmd = 'enable'
        elif cmd in ['destroy']:
            cmd = 'disable'
        return click.Group.get_command(self, ctx, cmd)

def set_docstring(f):
    @click.pass_context
    def new_func(ctx, *args, **kwargs):
        return ctx.invoke(f, ctx.obj, *args, **kwargs)
    return update_wrapper(new_func, f)

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

def require_arg(ctx, param, value):
    if not value:
        raise click.BadParameter("Must specify node(s).")
    return value

def validate_date_arg(ctx, param, value):
    if not value:
        return
    try:
        return gpstime.parse(value)
    except GPSTimeException:
        raise click.BadParameter("Could not parse date/time string: '%s'" % value)

##################################################

@click.group(cls=GuardctrlCommandGroup,
             context_settings=CONTEXT_SETTINGS,
             epilog="""\b
Node names may be specified with wildcard/globbing, e.g. 'SUS_*'.
A single '*' will act on all configured nodes (where appropriate).
""",
             )
@click.option('-d', '--debug', is_flag=True,
              help="Print debug information to stderr.")
@click.option('-v', '--version', is_flag=True, callback=print_version,
              expose_value=False, is_eager=True,
              help='Print version and exit.',
              )
@click.pass_context
def cli(ctx, debug=False):
    """Guardian daemon supervision control interface.

    Control guardian daemon processes managed by the site systemd
    supervision system.

    """
    if debug:
        logging.getLogger().setLevel('DEBUG')

##################################################

@cli.command('list', context_settings=CONTEXT_SETTINGS)
@click.argument('nodes', nargs=-1)
@click.option('-s', '--status', 'include_status', is_flag=True,
              help="include node service status")
@click.option('-r', '--running', 'only_active', is_flag=True,
              help="list only running/active nodes")
def list_nodes(nodes, include_status, only_active):
    """List nodes.

    NODES may be wildcard/glob patterns, in which case only matching
    nodes are returned (e.g. 'SUS_*'.  Default is all nodes, e.g. '*'.

    """
    if not nodes:
        nodes = '*'
    nodes = systemd.list_nodes(nodes,
                               only_active=only_active,
                               include_status=True)
    if not nodes:
        return
    node_w = len(max([node[0] for node in nodes])) + 4
    if include_status:
        fmt = '{{node:<{node_w}}} {{enabled:<{enabled_w}}} {{active:<{active_w}}}'.format(
            node_w=node_w,
            enabled_w=10,
            active_w=10,
        )
    else:
        fmt = '{node}'
    for node, enabled, active in nodes:
        kwargs = {}
        if active == 'failed':
            kwargs = dict(fg='red', bold=True)
        elif active != 'active':
            kwargs = dict(fg='yellow')
        elif active == 'active':
            kwargs = dict(fg='green')
        line = fmt.format(node=node, enabled=enabled, active=active)
        click.secho(line, **kwargs)


@cli.command('status', context_settings=CONTEXT_SETTINGS)
@click.argument('nodes', nargs=-1)
@functools.wraps(systemd.print_node_status)
def print_node_status(nodes):
    try:
        return systemd.print_node_status(nodes, print_logs=True)
    except util.GuardCtrlError as e:
        raise click.ClickException(e)


@cli.command('enable', context_settings=CONTEXT_SETTINGS)
@click.argument('nodes', nargs=-1, callback=require_arg)
@functools.wraps(systemd.enable_nodes)
def enable_nodes(nodes):
    try:
        return systemd.enable_nodes(nodes)
    except util.GuardCtrlError as e:
        raise click.ClickException(e)


@cli.command('start', context_settings=CONTEXT_SETTINGS)
@click.argument('nodes', nargs=-1, callback=require_arg)
@functools.wraps(systemd.start_nodes)
def start_nodes(nodes):
    try:
        return systemd.start_nodes(nodes)
    except util.GuardCtrlError as e:
        raise click.ClickException(e)


@cli.command('restart', context_settings=CONTEXT_SETTINGS)
@click.argument('nodes', nargs=-1, callback=require_arg)
@functools.wraps(systemd.restart_nodes)
def restart_nodes(nodes):
    try:
        return systemd.restart_nodes(nodes)
    except util.GuardCtrlError as e:
        raise click.ClickException(e)


@cli.command('stop', context_settings=CONTEXT_SETTINGS)
@click.argument('nodes', nargs=-1, callback=require_arg)
@functools.wraps(systemd.stop_nodes)
def stop_nodes(nodes):
    try:
        return systemd.stop_nodes(nodes)
    except util.GuardCtrlError as e:
        raise click.ClickException(e)


@cli.command('disable', context_settings=CONTEXT_SETTINGS)
@click.argument('nodes', nargs=-1, callback=require_arg)
@functools.wraps(systemd.disable_nodes)
def disable_nodes(nodes):
    node_list = systemd.list_nodes(nodes)
    if not node_list:
        raise click.ClickException("Unknown nodes: {}".format(' '.join(nodes)))
    click.echo("Really disable the following nodes?")
    for node in node_list:
        click.echo("  {}".format(node))
    click.echo("This will remove these nodes from the site list.")
    if not click.confirm("Disable?"):
        return
    try:
        return systemd.disable_nodes(node_list)
    except util.GuardCtrlError as e:
        raise click.ClickException(e)

##################################################

@cli.command('log', context_settings=CONTEXT_SETTINGS,
             epilog="""
Date/time strings for filters are handled by a date/time string parser
and therefore may be specified in various formats.  If --after and
--before are not both specified, --duration will be used.  --lines
will ultimately limit the number of lines returned.

Examples:

  Show all ISC_LOCK logs from Sept. 14, 2015:

\b
  $ {prog} --after 2015-09-14 --before 2015-09-15 ISC_LOCK

  Show the last 1000 lines of all SUS node logs:

\b
  $ {prog} -n1000 SUS_*

  Quote compound date strings, and specify 'UTC' for times in UTC:

\b
  $ {prog} -a "2016-08-20 06:23:22 UTC" -b "2016-08-20 06:23:52 UTC" ISC_LOCK

  GPS times are accepted as well:

\b
  $ {prog} 1155709419 ISC_DRMI

""".format(prog=PROG+' log'),
             )
@click.argument('nodes', nargs=-1)
@click.option('-f', '--follow', is_flag=True,
              help="Follow logs.")
@click.option('-a', '--after', metavar='DATETIME', callback=validate_date_arg,
              help="Filter for logs after specified date/time.")
@click.option('-b', '--before', metavar='DATETIME', callback=validate_date_arg,
              help="Filter for logs after specified date/time.")
#
# @click.option('-c', '--center', metavar='DATETIME', callback=validate_date,
#               help="filter for logs centered around specified time")
# @click.option('-o', '--hours', 'hours', type=int, metavar='HOURS',
#               help="filter for logs from the last H hours")
# @click.option('-t', '--day', 'hours', flag_value=24,
#               help="filter for logs from the last 24 hours")
# @click.option('-d', '--duration', metavar='SECONDS', type=float, default=60)
#
@click.option('-n', '--lines', 'nlines', metavar='N', type=int, default=100,
              help="Limit logs to N lines [100].")
@click.option('-u', '--utc', 'time_format', flag_value='utc', default='utc',
              help="Show log lines with UTC timestamps (default).")
@click.option('-l', '--local', 'time_format', flag_value='local',
              help="Show log lines with local timestamps.")
@click.option('-g', '--gps', 'time_format', flag_value='gps',
              help="Show log lines with GPS timestamps.")
@click.option('--old', is_flag=True,
              help="Search old logs (<=O2, some arguments ignored).")
def print_logs(**kwargs):
    """View node logs.

    NODES may be specified with wildcard/globbing, e.g. 'SUS_*'.
    Default is to show logs for all nodes, e.g. '*'.

    Log timestamps are in UTC by default.

    """
    # ddelta = timedelta(seconds=float(args.duration))

    # if carg('hours'):
    #     args.after = gpstime.now(tz=tzlocal()) - timedelta(seconds=3600*float(args.hours))
    # elif carg('after'):
    #     args.after = _parse_time_arg('after')
    #     if carg('before'):
    #         args.before = _parse_time_arg('before')
    #         if args.before < args.after:
    #             raise util.GuardCtrlError("Before time is earlier than after time.")
    #     elif carg('duration'):
    #         args.before = args.after + ddelta
    # elif carg('center'):
    #     dur = ddelta/2
    #     args.after = _parse_time_arg('center') - dur
    #     args.before = _parse_time_arg('center') + dur
    # elif carg('before'):
    #     args.before = _parse_time_arg('before')
    #     args.after = args.before - ddelta
    # # else:
    # #     args.follow = True

    # logging.debug("search after : %s" % args.after)
    # logging.debug("search before: %s" % args.before)

    if kwargs['time_format'] == 'utc':
        def dt2s(dt):
            return dt.astimezone(tzutc()).strftime(util.ISO_FMT+'Z')
    elif kwargs['time_format'] == 'local':
        def dt2s(dt):
            return dt.strftime(util.ISO_FMT)
    elif kwargs['time_format'] == 'gps':
        def dt2s(dt):
            return '{:f}'.format(gpstime.fromdatetime(dt).gps())

    if kwargs['old']:
        nkwargs = {k: kwargs[k] for k in ['nodes', 'after', 'before']}
        gen = svlogd.node_log_search(**nkwargs)
    else:
        nkwargs = {k: kwargs[k] for k in ['nodes', 'after', 'before', 'nlines', 'follow']}
        gen = systemd.node_logs(**nkwargs)

    for dt, msg in gen:
        line = '{} {}'.format(dt2s(dt), msg)
        click.echo(line)

##################################################

def main():
    signal.signal(signal.SIGPIPE, signal.SIG_DFL)

    FORMAT = '%(levelname)s: %(module)s: %(message)s'
    logging.basicConfig(format=FORMAT, level=os.getenv('LOG_LEVEL', 'INFO').upper())

    # handle invocation as ssh ForceCommand
    if os.getenv('SSH_ORIGINAL_COMMAND') and len(sys.argv) == 1:
        sys.argv += os.getenv('SSH_ORIGINAL_COMMAND').split()

    logging.debug(sys.argv)

    # handle direct systemctl/journalctl invocation
    if len(sys.argv) > 1 and \
       sys.argv[1] in ['env', 'systemctl', 'journalctl']:
        logging.debug("cmd: {}".format(' '.join(sys.argv[1:])))
        sys.exit(subprocess.run(sys.argv[1:]).returncode)

    cli(obj={})


if __name__ == '__main__':
    main()
