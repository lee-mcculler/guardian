import os
import logging

import guardian
from guardian.system import GuardSystem, GuardSystemError
from guardian.const import CAS_PREFIX_FMT
from guardian.db import guarddb


class GuardCtrlError(Exception):
    pass


ISO_FMT = '%Y-%m-%d_%H:%M:%S.%f'#%z'


def check_system(name):
    try:
        GuardSystem(name)
    except GuardSystemError as e:
        raise GuardCtrlError(e)


try:
    IFO = os.environ['IFO']
    SITE = os.environ['SITE']
except KeyError:
    raise RuntimeError("Must specify IFO and SITE environment variables.")
CHANFILE_DEFAULT = '/opt/rtcds/'+SITE.lower()+'/'+IFO.lower()+'/chans/daq/'+IFO+'EDCU_GRD.ini'
CHANFILE = os.getenv('GUARD_CHANFILE', CHANFILE_DEFAULT)
CHANFILE_HEADER = '''
[default]
gain=1.00
datatype=4
ifoid=0
slope=1.00
acquire=3
offset=0
units=V
dcuid=4
datarate=16
'''

def test_chanfile():
    tfile = CHANFILE
    try:
        open(tfile, 'a').close()
    except Exception as e:
        raise GuardCtrlError(e)

def update_global_chanfile(node_list):
    logging.info("updating global channel file: {}".format(CHANFILE))
    cdir = os.path.dirname(CHANFILE)
    if not os.path.exists(cdir):
        os.makedirs(cdir)
    tfile = CHANFILE+'.new'
    with open(tfile, 'w') as f:
        f.write(CHANFILE_HEADER.strip())
        f.write('\n\n')
        for node in node_list:
            for chan, attr in sorted(guarddb.items()):
                if not attr.get('archive'):
                    continue
                channel = CAS_PREFIX_FMT.format(IFO=IFO, SYSTEM=node) + chan
                f.write('[{}]\n'.format(channel))
    os.rename(tfile, CHANFILE)
