from __future__ import print_function
import os
import sys
import argparse
import tempfile
import traceback
import subprocess

import guardian.cli as cli
import guardian.const as const
from guardian.ligopath import ARCHIVE_ROOT

############################################################

class tcolors:
    CYAN = '\033[96m'
    MAGENTA = '\033[95m'
    BLUE = '\033[94m'
    YELLOW = '\033[93m'
    GREEN = '\033[92m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    RESET = '\033[0m'

def nds_connection():
    import nds2
    port = 31200
    NDSSERVER = os.getenv('NDSSERVER')
    if NDSSERVER:
        hostport = NDSSERVER.split(',')[0].split(':')
        host = hostport[0]
        if len(hostport) > 1:
            port = int(hostport[1])
    else:
        raise ValueError('NDSSERVER not specified')
    return nds2.connection(host, port)

############################################################

def phelp(args=None):
    """help"""
    parser.print_help()

def sprint(args):
    """print general system information"""
    system = cli.init_system(args, load=False)
    cli.print_system(system)

def states(args):
    """print state enumeration"""
    system = cli.init_system(args, load=True)
    cli.print_states(system, flag_requests=False,
                     requests_only=args.requests_only)

def edges(args):
    """print transition edges"""
    system = cli.init_system(args, load=True)
    for edge in system.graph.edges_iter(data=True):
        source, sink, data = edge
        print('({0}, {1}, {2})'.format(source, sink, data['weight']))

def path(args):
    """print system graph path as a list of states"""
    system = cli.init_system(args, load=True)
    for state in system.shortest_path(args.state0, args.state1):
        print(state)

def graph(args):
    """draw/display system graph"""
    try:
        import guardian.graph as graph
    except ImportError as e:
        sys.exit("Graph drawing not supported: %s" % e)
    system = cli.init_system(args, load=True)
    if args.all:
        args.gotos = True
        args.jumps = True
    path = ()
    if args.state:
        if args.request:
            path = (args.state, args.request)
        else:
            path = (args.state,)
    if args.query:
        try:
            from guardian.manager import Node, NodeConnectError
        except ImportError as e:
            sys.exit("Node query not supported: %s" % e)
        try:
            node = Node(system.name)
            node.init()
        except NodeConnectError as e:
            sys.exit("CA error: %s" % e)
        if node.request == 'NONE':
            path = (node.state,)
        else:
            path = (node.state, node.request)
    for state in path:
        if state not in system:
            sys.exit("State '%s' not defined in system %s." % (state, system.name))

    dot = graph.sys2dot(system,
                        path=path,
                        show_index=args.index,
                        show_gotos=args.gotos,
                        show_jumps=args.jumps,
                        edge_constraints=args.constraints,
                        )

    # display graph or write to file
    default_format = 'pdf'
    if args.outfile is not False:
        outfile = '%s_%s' % (system.ifo, system.name)
        outpath = args.outfile
        if outpath is None:
            outpath = outfile
        elif os.path.isdir(os.path.expanduser(outpath)):
            outpath = os.path.join(os.path.expanduser(outpath), outfile)
        ext = os.path.splitext(outpath)[1]
        if args.format:
            fmt = args.format
        elif ext != '':
            fmt = ext.strip('.')
        else:
            fmt = default_format
        if ext == '':
            outpath += '.'+fmt
        dot.write(outpath, format=fmt)
    else:
        if not args.format:
            args.format = default_format
        suffix = '.%s' % args.format
        if args.format == 'pdf':
            viewer = ['evince', '-w']
        elif args.format == 'svg':
            viewer = ['inkview']
            #viewer = ['iceweasel']
        elif args.format == 'dot':
            viewer = ['xdot']
        else:
            viewer = ['xdg-open']
        try:
            # FIXME: better temp file name
            with tempfile.NamedTemporaryFile(suffix=suffix, delete=False) as f:
                dot.write(f.name, format=args.format)
                try:
                    subprocess.call(viewer + [f.name])
                except OSError as e:
                    sys.exit("Graph viewer '%s' could not be found." % (viewer[0]))
        except KeyboardInterrupt:
            sys.exit()

def files(args):
    """print paths to all usercode source files"""
    system = cli.init_system(args, load=True)
    print(system.path)
    for path in system.usercode:
        print(path)

def source(args):
    """print usercode source lines for system object"""
    import inspect
    system = cli.init_system(args, load=True)
    try:
        obj = system.module.__dict__[args.object]
    except KeyError as e:
        sys.exit("Unknown system object: %s" % e)
    sf = inspect.getsourcefile(obj)
    lines, sl = inspect.getsourcelines(obj)
    el = sl + len(lines) - 1
    nwidth = len(str(sl + len(lines)))
    if not args.raw:
        print("object: %s" % (obj))
        print("  file: %s" % (sf))
        print(" lines: %d-%d" % (sl, el))
        print('-' * (nwidth + len(lines[0])))
    prefix = ''
    for line in lines:
        if not args.raw:
            prefix = \
                tcolors.GREEN + \
                '{sl:{nwidth}}'.format(sl=sl, nwidth=nwidth) + \
                tcolors.CYAN + \
                ':' + \
                tcolors.RESET
            sl += 1
        print('%s%s' % (prefix, line), end='')

def edit(args):
    """edit system usercode"""
    try:
        system = cli.init_system(args, load=True)
    except:
        print("WARNING: Exception encountered when loading module:\n", file=sys.stderr)
        print(traceback.format_exc(), file=sys.stderr)
        system = cli.init_system(args, load=False)
    cmd = []
    if args.editor:
        cmd = args.editor.split()
    elif os.getenv('EDITOR'):
        cmd = os.getenv('EDITOR').split()
    else:
        # try to find XDG-specified handler for text/x-python
        from gi.repository import Gio
        mimetype = 'text/x-python'
        # XDG DATA directories
        # http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
        xdg_share = [os.getenv('XDG_DATA_HOME', os.path.expanduser('~/.local/share'))] \
                    + os.getenv('XDG_DATA_DIRS', '/usr/local/share:/usr/share').split(':')
        # get desktop handler for mimetype
        desktop = subprocess.check_output(['xdg-mime', 'query', 'default', mimetype]).strip()
        # look for desktop file
        if desktop:
            for d in xdg_share:
                path = os.path.join(d, 'applications', desktop)
                if not os.path.exists(path):
                    continue
                # extract executable
                app = Gio.DesktopAppInfo.new_from_filename(path)
                exe = app.get_executable()
                if exe:
                    cmd = [exe]
                    break
    if not cmd:
        cmd = ['emacs']
    if args.object:
        import inspect
        try:
            obj = system.module.__dict__[args.object]
        except KeyError as e:
            sys.exit("Unknown system object: %s" % e)
        source_file = inspect.getsourcefile(obj)
        source_lines, line_start = inspect.getsourcelines(obj)
        line_end = line_start + len(source_lines)
        #cmd += ['+%d' % sl, sf]
        if 'emacs' in cmd[0]:
            cmd += [source_file,
                    '--eval',
                    "(narrow-to-region (line-beginning-position {}) (line-end-position {}))".format(line_start, line_end),
                    ]
        elif 'gedit' in cmd[0]:
            cmd += [source_file, '+{}'.format(line_start)]
        else:
            sys.exit("Object editing only supported with emacs and gedit.")
    else:
        cmd += [system.path]
        if system.usercode:
            cmd += system.usercode
    print(' '.join(cmd), file=sys.stderr)
    try:
        try:
            subprocess.call(cmd)
        except OSError:
            sys.exit("Could not find editor: %s" % cmd[0])
    except KeyboardInterrupt:
        sys.exit()

def archive_clone(args):
    """clone system usercode git archive for inspection

    The GUARD_ARCHIVE_ROOT environment variable points to the root
    directory where all system archives are stored.  This may point to
    at network location as well (e.g. https://...).

    """
    import git
    import shutil
    import tempfile
    import guardian.archive as archive

    archive_path = os.path.join(ARCHIVE_ROOT, args.system)

    if '://' not in archive_path:
        archive_path = os.path.abspath(archive_path)

    tempdir = None
    if args.directory:
        if not os.path.exists(args.directory):
            os.mkdir(args.directory)
        git.Git(args.directory).clone(archive_path)
    else:
        tempdir = tempfile.mkdtemp(prefix='guardutil_archive_%s_' % (args.system))
        try:
            wd = os.path.join(tempdir, args.system)
            os.mkdir(wd)
            #git.Repo.clone_from(archive_path, root)
            git.Git(tempdir).clone(archive_path)
            os.chdir(wd)
            if args.shell:
                subprocess.call('bash')
            else:
                subprocess.call('gitk')
        except KeyboardInterrupt:
            pass
        finally:
            if tempdir:
                shutil.rmtree(tempdir)

def plot(args):
    """plot node data/state around specified time (NDS)"""
    import matplotlib.pyplot as plt
    import guardian.plotutils as pu
    system = cli.init_system(args, load=True)
    center_time = args.time0
    window = eval(args.window)
    pu.guardfig(system, center_time, window)

def state_hist(args):
    """print state history around specified time (NDS)"""
    from gpstime import gpstime
    system = cli.init_system(args, load=True)
    channel = const.CAS_PREFIX_FMT.format(system.ifo, system.name) + 'STATE_N'
    if args.time1:
        t0 = int(gpstime.parse(args.time0).gps())
        t1 = int(gpstime.parse(args.time1).gps())
    else:
        ct = gpstime.parse(args.time0)
        window = eval(args.window)
        t0 = int(ct.gps() + window[0])
        t1 = int(ct.gps() + window[1])
    conn = nds_connection()
    buf = conn.fetch(t0, t1, [channel])
    buf = buf[0]
    state_array = buf.data

    # some helper function
    def i2t(i):
        """gps time of NDS data index"""
        return buf.gps_seconds + i*(1./buf.channel.sample_rate)
    def stime(gt):
        """gpstime string"""
        if args.gps:
            return '%.6f' % gt.gps()
        else:
            return gt.iso()
    def ptrans(last, cur):
        """print transition"""
        print('{time} {frm} -> {to}'.format(
            time=stime(cur[1]), frm=last[0], to=cur[0]))
    def pdur(last, cur):
        """print duration"""
        print('{state:{msl}s} {ent} -> {ext} ({dur})'.format(
            msl=msl, state=last[0],
            ent=stime(last[1]), ext=stime(cur[1]), dur=(cur[1] - last[1])))

    # find all transitions
    trans = []
    last = None
    for i, s in enumerate(state_array):
        if s == last:
            continue
        trans.append((
            system.index(int(s)),
            gpstime.fromgps(i2t(i)),
            ))
        last = s
    # record the last sample
    trans.append((
        system.index(int(s)),
        gpstime.fromgps(i2t(i)),
        ))
    msl = max([len(s) for s,t in trans])
    # print the results
    last = trans[0]
    for cur in trans[:-1]:
        if cur == last:
            continue
        if args.transitions:
            ptrans(last, cur)
        else:
            pdur(last, cur)
        last = cur
    if not args.transitions:
        pdur(last, trans[-1])


############################################################

PROG = 'guardutil'
description = '''Advanced LIGO Guardian system utility.'''
epilog = """

Add '-h' after individual commands for command help.

Environment variables:
  IFO                       IFO designator (required)
  USERAPPS_DIR              LIGO "userapps" root path [/opt/rtcds/userapps/release]
  GUARD_MODULE_PATH         Override default userapps system module search path
  GUARD_ARCHIVE_ROOT        System archive root directory (may be URL)
"""

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    prog=PROG,
    description=description,
    epilog=epilog,
    )

subparser = parser.add_subparsers(
    #title='Commands',
    metavar='<command>',
    #description='''subcommand (see "<cmd> -h" for specific usage).''',
    dest='cmd',
    #help=argparse.SUPPRESS,
    )
subparser.required = True

sp = {}

def gen_subparser(cmd, func):
    help = func.__doc__.split('\n')[0]
    desc = func.__doc__
    p = subparser.add_parser(cmd,
                             formatter_class=argparse.RawDescriptionHelpFormatter,
                             help=help,
                             description=desc)
    p.set_defaults(func=func)
    return p

cmd = 'print'
func = sprint
sp[cmd] = gen_subparser(cmd, func)
cli.add_grd_arg(sp[cmd], 'system')

cmd = 'states'
func = states
sp[cmd] = gen_subparser(cmd, func)
cli.add_grd_arg(sp[cmd], 'system')
sp[cmd].add_argument('-r', '--requests-only', action='store_true',
                     help="output request states only")

cmd = 'edges'
func = edges
sp[cmd] = gen_subparser(cmd, func)
cli.add_grd_arg(sp[cmd], 'system')

cmd = 'path'
func = path
sp[cmd] = gen_subparser(cmd, func)
cli.add_grd_arg(sp[cmd], 'system')
sp[cmd].add_argument('state0', help="initial state")
sp[cmd].add_argument('state1', help="final state")

cmd = 'files'
func = files
sp[cmd] = gen_subparser(cmd, func)
cli.add_grd_arg(sp[cmd], 'system')

cmd = 'graph'
func = graph
sp[cmd] = gen_subparser(cmd, func)
cli.add_grd_arg(sp[cmd], 'system')
sp[cmd].add_argument('-i', '--index', action='store_true',
                     help="add state indices to labels")
sp[cmd].add_argument('-g', '--gotos', action='store_true',
                     help="draw 'goto' edges")
sp[cmd].add_argument('-j', '--jumps', action='store_true',
                     help="draw 'jump' edges")
sp[cmd].add_argument('-a', '--all', action='store_true',
                     help="show all edges (-gj)")
sp[cmd].add_argument('-c', '--constraints', action='store_true',
                     help="use edge constraints for goto and jump edges")
sp[cmd].add_argument('-q', '--query', action='store_true',
                     help="query via EPICS for <state> and <request> from running guard node for highlighting path")
sp[cmd].add_argument('-f', '--format', metavar='<type>', type=str,
                     help="drawing format: 'pdf', 'svg', etc. (default: 'pdf')")
sp[cmd].add_argument('-o', '--outfile', metavar='<path>', type=str, nargs='?', default=False,
                     help="save graph to file.  If path is a directory, a file named \"<system name>.<format>\" will be saved in that directory.  If path includes extension the extension will be used to determine format.")
sp[cmd].add_argument('state', metavar='<state>', type=str, nargs='?',
                     help="state to highlight")
sp[cmd].add_argument('request', metavar='<request>', type=str, nargs='?',
                     help="final (request) state for highlighting path")

cmd = 'source'
func = source
sp[cmd] = gen_subparser(cmd, func)
cli.add_grd_arg(sp[cmd], 'system')
sp[cmd].add_argument('object', metavar='<object>',
                     help="code object")
sp[cmd].add_argument('-r', '--raw', action='store_true',
                     help="raw source, without header or line numbers")

cmd = 'edit'
func = edit
sp[cmd] = gen_subparser(cmd, func)
cli.add_grd_arg(sp[cmd], 'system')
sp[cmd].add_argument('object', metavar='<object>', type=str, nargs='?',
                     help="object code to jump to in editor")
sp[cmd].add_argument('-e', '--editor', type=str,
                     help="editor (emacs, gedit, etc. (default: xdg-open))")

cmd = 'archive-clone'
func = archive_clone
sp[cmd] = gen_subparser(cmd, func)
# requires name only (not path) so create separately
sp[cmd].add_argument('system', metavar='<system>',
                     help="Guardian system name")
sp[cmd].add_argument('directory', metavar='<directory>', nargs='?',
                     help="checkout directory")
sp[cmd].add_argument('-s', '--shell', action='store_true',
                     help="open shell in checked-out repo")

cmd = 'plot'
func = plot
sp[cmd] = gen_subparser(cmd, func)
sp[cmd].add_argument('-w', '--window', metavar='<window>', default='[-60, 60]',
                     help="time window if time1 not specified (default: [-60, 60])")
cli.add_grd_arg(sp[cmd], 'system')
sp[cmd].add_argument('time0', metavar='<time0>',
                     help="center or start time")
sp[cmd].add_argument('time1', metavar='<time1>', nargs='?',
                     help="duration or stop time")

cmd = 'state-hist'
func = state_hist
sp[cmd] = gen_subparser(cmd, func)
sp[cmd].add_argument('-t', '--transitions', action='store_true',
                     help="show state transitions rather than duration")
sp[cmd].add_argument('-g', '--gps', action='store_true',
                     help="show times in GPS")
sp[cmd].add_argument('-w', '--window', metavar='<window>', default='[-60, 60]',
                     help="time window if time1 not specified (default: [-60, 60])")
cli.add_grd_arg(sp[cmd], 'system')
sp[cmd].add_argument('time0', metavar='<time0>',
                     help="center or start time")
sp[cmd].add_argument('time1', metavar='<time1>', nargs='?',
                     help="duration or stop time")

############################################################

def main():
    args = parser.parse_args()
    args.func(args)

if __name__ == '__main__':
    main()
