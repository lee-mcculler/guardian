import os
import numpy as np
from gpstime import gpstime
from guardian import GuardSystem

def nds_connection():
    import nds2
    port = 31200
    NDSSERVER = os.getenv('NDSSERVER')
    if NDSSERVER:
        hostport = NDSSERVER.split(',')[0].split(':')
        host = hostport[0]
        if len(hostport) > 1:
            port = int(hostport[1])
    else:
        raise ValueError('NDSSERVER not specified')
    return nds2.connection(host, port)

def find_transitions(system, t0, t1):
    """yield all node state transitions between specified times

    """
    system = GuardSystem(system)
    system.load()
    t0 = int(gpstime.parse(t0).gps())
    t1 = int(gpstime.parse(t1).gps())

    channel = '%s:GRD-%s_STATE_N' % (system.ifo, system.name)
    conn = nds_connection()

    last_state = -2
    for buf in conn.iterate(t0, t1, [channel]):
        buf = buf[0]
        state_array = buf.data.astype(int)
        previous_last_state = last_state
        last_state = state_array[-1]

        # FIXME: need to check that state index hasn't changed in this
        # block

        # gps time of NDS data index
        def i2t(i):
            return buf.gps_seconds + i*(1./buf.channel.sample_rate)
        # return state name for index
        def i2s(i):
            if i == -1:
                if previous_last_state == -2:
                    s = state_array[0]
                else:
                    s = previous_last_state
            else:
                s = state_array[i]
            return system.index(s)

        # calculate indicies on either side of all transitions
        grad = np.gradient(
            np.concatenate((
                    np.array((previous_last_state,)),
                    state_array)))
        transi = np.where(grad != 0)[0] - 1
        # if there are no transitions in this block continue
        if not transi.any():
            continue

        trans = []
        lasti = -1
        for i in transi:
            if i == lasti + 1 and i2s(i) != i2s(lasti):
                st = (i2s(i-1), i2s(i))
                t = (st, gpstime.fromgps(i2t(i)))
                yield t
            lasti = i
    return
