# -*- makefile -*-

PACKAGE = guardian

VERSION := $(shell git describe | sed -e 's/_/~/' -e 's/-/+/' -e 's/-/~/' -e 's|.*/||')
VFILE := lib/guardian/_version.py
CVERS := $(shell test -e $(VFILE) && python -c "execfile('$(VFILE)'); print __version__")
DIST := $(PACKAGE)-$(VERSION)

##########

.PHONY: all
all: build

.PHONY: test
test:
	./test/guardian-test $(GUARD_TEST_OPTS)
test-verbose:
	./test/guardian-test --verbose $(GUARD_TEST_OPTS)

##########

.PHONY: docs
docs:
	$(MAKE) -C docs html
	$(MAKE) -C docs latexpdf

##########

.PHONY: version _release-commit release-major release-minor release-rev release
version:
ifneq ($(VERSION), $(CVERS))
	echo "__version__ = '$(VERSION)'" >$(VFILE)
else
	echo "__version__ = '$(VERSION)'"
endif
_release-commit: version
	git add $(VFILE)
	git commit -m "Release $(VERSION)"
	git tag -m "Release" $(VERSION)
release-major: VERSION = $(shell PYTHONPATH=lib/guardian python -c \
	"import version as v; print v._release('$(VERSION)', 'major')")
release-major: _release-commit
release-minor: VERSION = $(shell PYTHONPATH=lib/guardian python -c \
	"import version as v; print v._release('$(VERSION)', 'minor')")
release-minor: _release-commit
release-rev: VERSION = $(shell PYTHONPATH=lib/guardian python -c \
	"import version as v; print v._release('$(VERSION)', 'rev')")
release-rev: _release-commit
release: release-rev

##########

.PHONY: dist
dist: version
	python ./setup.py sdist

.PHONY: build
build: version
	python ./setup.py build

.PHONY: install
ifndef PREFIX
install: version
	python ./setup.py install
else
install: version
	python ./setup.py install --prefix=$(PREFIX)
endif

##################################################

ligo-install: APPSROOT = $(or $(APPSROOT), /ligo/apps/linux-x86_64)
ligo-install: DISTDIR = $(APPSROOT)/$(PACKAGE)-$(VERSION)
ligo-install: ENVSCRIPT = etc/guardian-user-env.sh
ligo-install: version
	python ./setup.py install --prefix=$(DISTDIR)
	install -d $(DISTDIR)/etc
	install $(ENVSCRIPT) $(DISTDIR)/etc
	sed -i "s|___GUARDIAN_LOCATION___|$(DISTDIR)|" $(DISTDIR)/$(ENVSCRIPT)

##########

# Debian/Ubuntu test package
.PHONY: deb-snapshot
# FIXME: do this without creating a dist tarball
deb-snapshot: dist
	mkdir -p build
	cd build; tar zxf ../dist/$(DIST).tar.gz
	cp -a debian build/$(DIST)
	cd build/$(DIST); dch -b -v $(VERSION) -D UNRELEASED 'test build, not for upload'
	cd build/$(DIST); echo '3.0 (native)' > debian/source/format
	cd build/$(DIST); debuild -us -uc

.PHONY: deb dsc
deb-src: VERSION = $(shell git describe master)
deb-src: DIST = $(PACKAGE)-$(VERSION)
deb-src:
	rm -rf build/$(VERSION)
	rm -rf build/$(DIST)
	mkdir -p build/$(VERSION)
	git archive master | tar -x -C build/$(VERSION)/
	cd build/$(VERSION) && python setup.py sdist
	mv build/$(VERSION)/dist/$(DIST).tar.gz build/$(PACKAGE)_$(VERSION).orig.tar.gz
	cd build && tar xf $(PACKAGE)_$(VERSION).orig.tar.gz
	mkdir build/$(DIST)/debian
	git archive debian:debian | tar -x -C build/$(DIST)/debian/
dsc: VERSION = $(shell git describe master)
dsc: DIST = $(PACKAGE)-$(VERSION)
dsc: deb-src
	cd build/$(DIST) && dpkg-source -b .
deb: VERSION = $(shell git describe master)
deb: DIST = $(PACKAGE)-$(VERSION)
deb: deb-src
	cd build/$(DIST) && debuild -uc -us

##########

.PHONY: clean
clean:
	rm -rf dist
	rm -rf build
	rm -rf test/tmp.*
