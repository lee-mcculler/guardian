.. _ezca:

:class:`Ezca` EPICS Channel Access
==================================

The :class:`Ezca` object

initialization
--------------


read/write channel access
-------------------------


LIGO standard filter modules
----------------------------





.. autoclass:: ezca.Ezca
   :members:

      
