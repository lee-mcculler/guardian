Release 1.0.0
=============

First release with the new versioning scheme.

* improved guardlog client/server (support node wildcards).
* MEDM control screen improvements
* more informative daemon logging
