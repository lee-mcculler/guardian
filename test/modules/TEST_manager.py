from guardian import GuardState
from guardian import NodeManager

nodes = NodeManager(['TEST'])

class INIT(GuardState):
    def main(self):
        nodes.set_managed()

    @nodes.checker(fail_return='FAULT')
    def run(self):
        return True

class A(GuardState):
    def main(self):
        nodes['TEST'] = 'A'

    @nodes.checker(fail_return='FAULT')
    def run(self):
        return nodes.arrived

class FAULT(GuardState):
    @nodes.checker(fail_return=False)
    def run(self):
        return 'INIT'
    
edges = [
    ('INIT', 'A'),
    ]
