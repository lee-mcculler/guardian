from guardian import GuardState

class A(GuardState):
    pass

class B(GuardState):
    pass

class C(GuardState):
    pass

edges = [
    ('INIT', 'A'),
    ('A', 'B'),
    ('B', 'C'),
    ]
