from guardian import GuardState

class INIT(GuardState):
    def run(self):
        return 'A'

class A(GuardState):
    index = 1
    goto = True
    def main(self):
        self.log('A')
        self.i = 0
        ezca['A'] = self.i

    def run(self):
        if ezca['J'] != 0:
            return 'J'
        self.i += 1
        ezca['A'] = self.i
        if self.i >= 4:
            return True

class B(GuardState):
    index = 2
    request = False
    def main(self):
        self.log('B')
        ezca['B'] = 1
        ezca['B'] = 2
        ezca['B'] = 3

class C(GuardState):
    index = 3
    def main(self):
        if ezca['C'] == 10:
            return 'J'
        ezca['C'] = 0

    def run(self):
        if ezca['J'] != 0:
            return 'J'
        self.log('C')
        if ezca['C'] < 4:
            ezca['C'] += 1
        else:
            return True

class J(GuardState):
    request = False
    redirect = False
    def run(self):
        if ezca['K'] != 0:
            return True

edges = [
    ('A', 'B'),
    ('B', 'C'),
    ('J', 'A'),
    ]
