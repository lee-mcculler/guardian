#!/usr/bin/env bash

# Run tests
#
# Copyright (c) 2005 Junio C Hamano
#
# Adapted from a Makefile to a shell script by Carl Worth (2010)

if [ ${BASH_VERSINFO[0]} -lt 4 ]; then
    echo "Error: The guardian test suite requires a bash version >= 4.0"
    echo "due to use of associative arrays within the test suite."
    echo "Please try again with a newer bash (or help us fix the"
    echo "test suite to be more portable). Thanks."
    exit 1
fi

cd $(dirname "$0")

TESTS="
  basic
  system
  imports
  cli
  graph
  daemon
  jump
  redirect
  manager
  archive
"

#  setup
TESTS=${GUARD_TESTS:=$TESTS}

# test for timeout utility
if command -v timeout >/dev/null; then
    TEST_TIMEOUT_CMD="timeout 1m "
    echo "INFO: using 1 minute timeout for tests"
else
    TEST_TIMEOUT_CMD=""
fi

# Prep
rm -rf test-results
rm -rf tmp.*

trap 'e=$?; kill $!; exit $e' HUP INT TERM
# Run the tests
for test in $TESTS; do
    $TEST_TIMEOUT_CMD ./$test "$@" &
    wait $!
    # If the test failed without producing results, then it aborted,
    # so we should abort, too.
    RES=$?
    if [[ $RES != 0 && ! -e "test-results/${test%.sh}" ]]; then
	echo
	echo "FAIL: script '$test' returned $RES"
	exit $RES
    fi
done
trap - HUP INT TERM

# Report results
echo
./lib/test-aggregate-results test-results/*
ev=$?

# Clean up
rm -rf test-results

exit $ev
