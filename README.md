Guardian Automation
===================

Design by and for the Advanced LIGO project, Guardian is a flexible,
modular, distributed, hierarchical, state machine automation platform.
It is not specific to aLIGO, though; it should be useful for an large
or small experiment that could benefit from state-machine supervisory
control.

The objective of Guardian is to provide:
 * a framework for complete, robust automation
 * a flexible interface that facilitates the commissioning process
 * useful diagnostics and coherent tracking of the full state of the
   system

Concept:
--------

Individual automaton nodes oversee well defined sub-domains of the
interferometer.  A hierarchy of nodes can be used to control larger
systems.

Features:
---------

User code is written in standard Python, and is very simple and
straightforward to write.

Includes many useful command line and graphical utilities:
 * automatically draw system graphs
 * analyze system graphs
 * inspect/edit user code
 * directly execute code for individual states, or for arbitrary state graph paths
 * interactive guardian shell environment
 * plot guardian status over time
